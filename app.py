from flask import Flask
from prometheus_flask_exporter import PrometheusMetrics
import os

app = Flask(__name__)
metrics = PrometheusMetrics(app)

metrics.info('app_info','Teknik Pemrograman', version = '0.0.1')

@app.route('/')
@metrics.do_not_track()
@metrics.counter('request_by_ip' , 'Number of request per ip', labels= {'ip_address': lambda: request.remote_addr})
def index():
	return 'Hello world latihan teknik pemrograman 2019-07-11'

if __name__ == '__main__':
	app.run(host='0.0.0.0', port=os.environ.get('PORT', 5000))
