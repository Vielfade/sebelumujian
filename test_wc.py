import unittest
from wc import WC

class TestWC(unittest.TestCase):
    def test_wc(self):
        n = WC('README.md')
        self.assertEqual(506, n.word_count())
        self.assertEqual(68, n.line_count())
        self.assertEqual(5310, n.char_count())
